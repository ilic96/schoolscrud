   <b>Zadatak:</b>
   Kreirati REST API za rad sa školama. Škola je opisana poljima: Id, Ime, Mesto, Godina početka rada.<br/>
   Omogućiti CRUD operacije za rad sa školama. Napraviti kolekciju škola sa kojima će se raditi.<br/>
   Omogućiti prikaz onih škola čija je godina početka rada nakon godine koja se prosledi kroz telo zahteva.<br/>