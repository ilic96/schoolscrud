﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SchoolsCRUD.Models
{
    public class SchoolsCRUDContext : DbContext
    {
        public SchoolsCRUDContext() : base("name=SchoolContext")
        {

        }
        public DbSet<School> Schools { get; set; }
    }
}