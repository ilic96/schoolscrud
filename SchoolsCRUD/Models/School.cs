﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolsCRUD.Models
{
    public class School
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public int Year { get; set; }
    }
}