﻿using SchoolsCRUD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolsCRUD.Interfaces
{
    public interface ISchoolRepository
    {
        IEnumerable<School> GetAll();
        School GetById(int id);
        void Add(School school);
        void Update(School school);
        void Delete(School school);
    }
}
