﻿using SchoolsCRUD.Models;
using SchoolsCRUD.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SchoolsCRUD.Controllers
{
    public class SchoolsController : ApiController
    {
        SchoolRepository _repository = new SchoolRepository();

        //GET api/schools
        public IEnumerable<School> GetSchools()
        {
            return _repository.GetAll();
        }
        //GET api/schools/1
        public IHttpActionResult GetSchool(int id)
        {
            var school = _repository.GetById(id);
            if (school == null)
            {
                return BadRequest();
            }
            return Ok(school);
        }
        //POST api/schools/1
        public IHttpActionResult PostSchool(School school)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _repository.Add(school);
            
            return CreatedAtRoute("DefaultApi", new { id = school.Id }, school);
        }
        //PUT api/schools/1
        public IHttpActionResult PutSchool(School school, int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if(id != school.Id)
            {
                return NotFound();
            }
            try
            {
                _repository.Update(school);
            }
            catch
            {
                return BadRequest();
            }
            return Ok(school);
            
        }
        //DELETE api/schools/1
        public IHttpActionResult DeleteSchool(int id)
        {
            var school = _repository.GetById(id);
            if(school == null)
            {
                return NotFound();
            }
            _repository.Delete(school);
            return Ok();
        }

    }
}
