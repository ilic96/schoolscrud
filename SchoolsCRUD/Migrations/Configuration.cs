namespace SchoolsCRUD.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<SchoolsCRUD.Models.SchoolsCRUDContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SchoolsCRUD.Models.SchoolsCRUDContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            context.Schools.AddOrUpdate
                (
                    new Models.School() { Id = 1, Name = "Sveti Sava", City = "Sremska Mitrovica", Year = 2000},
                    new Models.School() { Id = 2, Name = "Pinki", City = "Novi Sad", Year = 2001},
                    new Models.School() { Id = 3, Name = "Sonja Marinkovic", City = "Beograd", Year = 1999}
                );
            context.SaveChanges();
        }
    }
}
