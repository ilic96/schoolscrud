﻿using SchoolsCRUD.Interfaces;
using SchoolsCRUD.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SchoolsCRUD.Repository
{
    public class SchoolRepository : ISchoolRepository
    {
        private SchoolsCRUDContext db = new SchoolsCRUDContext();

        public void Add(School school)
        {
            db.Schools.Add(school);
            db.SaveChanges();
        }

        public void Delete(School school)
        {
            db.Schools.Remove(school);
            db.SaveChanges();
        }

        public IEnumerable<School> GetAll()
        {
            return db.Schools;
        }

        public School GetById(int id)
        {
            return db.Schools.FirstOrDefault(s => s.Id == id);
        }

        public void Update(School school)
        {
            db.Entry(school).State = System.Data.Entity.EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DBConcurrencyException)
            {
                throw;
            }
        }
        
    }
}